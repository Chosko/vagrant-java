vagrant-jekyll
=======

This repo contains an automatic installation and configuration of Vagrant with jekyll environment.

To obtain a fully configured environment follow the steps below:

- Install [git](http://git-scm.com)
- Install [Ruby 2](https://www.ruby-lang.org/it) (maybe you want to use [rbenv](https://github.com/sstephenson/rbenv) or [rvm](https://rvm.io))
- Install [Vagrant](http://www.vagrantup.com/downloads)
- Install [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
- Set projects.yaml with the projects you want to include
```yaml
projects:
    my_first_project: 
        git: "https://github.com/username/my_first_project.git"
        port: 3000
    my_second_project:
        git: "https://github.com/username/my_second_project.git"
        port:3001
```
- Build the environment (it takes some minutes, just sit and wait!):
```
vagrant up
```
- log into the virtual machine:
```
vagrant ssh
```
- enjoy vagrant_jekyll apps in "/vagrant/apps"