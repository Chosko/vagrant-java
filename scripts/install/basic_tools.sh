  echo "[basic_tools] Installing basic tools..."

  # Updating repos
  apt-get update -y

  # Installing basic tools
  apt-get install -y curl
  apt-get install -y build-essential
  apt-get install -y vim
  apt-get install -y git
  apt-get install -y git-flow
  apt-get install tofrodos

  apt-get update -y

  echo "[basic_tools] Basic tools installed!"