# Installing rvm and ruby 2
echo "[java_installation] Installing JAVA"
apt-get install python-software-properties -y
add-apt-repository ppa:webupd8team/java -y
apt-get update -y
echo "[java_installation] Done..."
echo ""
echo "******************"
echo "*** IMPORTANT! ***"
echo "******************"
echo "to complete the JAVA installation run these commands at your first access:"
echo "sudo apt-get install oracle-java8-installer"
echo "sudo apt-get install oracle-java8-set-default"
