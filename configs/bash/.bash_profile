# coloured prompt
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "

# color on cli
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# git completion
source ~/.git-completion.bash
source ~/.git-flow-completion.bash

function pskill { 
	PSLINE=$(ps aux | grep $1 | grep -v grep)
	IFS=' ' read -ra ADDR <<< $PSLINE
	if [[ -n ${ADDR[1]} ]]; then
		kill -9 ${ADDR[1]}
		echo $1 killed
	else
		echo process $1 not found
	fi
}
